
/// link to the problem : https://codeforces.com/problemset/problem/438/D








#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e5 + 8;
const int maxlog = 20;
const ll inf = 1e18;
const int mod = 1e9 + 7;
const ld pi=3.1415926535897932384626433832795;
ll sum[4 * maxn];
int maxi[4 * maxn];
int a[maxn];
void build(int ind , int l , int r){
    if(r - l < 2){
        sum[ind] = (ll) a[l];
        maxi[ind] = l;
        return ;
    }
    int mid = (l + r) / 2;
    build(2 * ind , l , mid);
    build(2 * ind + 1 , mid , r);
    sum[ind] = sum[2 * ind] + sum[2 * ind + 1];
    if(a[maxi[2 * ind]] > a[maxi[2 * ind + 1]])
        maxi[ind] = maxi[2 * ind];
    else
        maxi[ind] = maxi[2 * ind + 1];
}
ll get(int ind , int l , int r , int x , int y){
    if(y <= l || r <= x)
        return 0;
    if(x <= l && r <= y)
        return sum[ind];
    int mid = (l + r) / 2;
    return get(2 * ind , l , mid , x , y) + get(2 * ind + 1 , mid , r , x , y);
}
int get_max(int ind , int l , int r , int x , int y){
    if(y <= l || r <= x)
        return 0;
    if(x <= l && r <= y)
        return maxi[ind];
    int mid = (l + r) / 2;
    int pl1 = get_max(2 * ind , l , mid , x , y);
    int pl2 = get_max(2 * ind + 1 , mid , r , x , y);
    if(a[pl1] > a[pl2])
        return pl1;
    else
        return pl2;
}
void update(int ind , int l , int r , int x){
    if(r - l < 2){
        sum[ind] = (ll) a[l];
        maxi[ind] = l;
        return ;
    }
    int mid = (l + r) / 2;
    if(x < mid)
        update(2 * ind , l , mid , x);
    else
        update(2 * ind + 1 , mid , r , x);
    sum[ind] = sum[2 * ind] + sum[2 * ind + 1];
    if(a[maxi[2 * ind]] > a[maxi[2 * ind + 1]])
        maxi[ind] = maxi[2 * ind];
    else
        maxi[ind] = maxi[2 * ind + 1];
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , m;
    cin >> n >> m;
    for(int i=1;i<=n;i++)
        cin >> a[i];
    build(1 , 1 , n + 1);
    while(m -- ){
        int type;
        cin >> type;
        if(type == 1){
            int l , r;
            cin >> l >> r;
            r ++ ;
            cout << get(1 , 1 , n + 1 , l , r) << endl;
        }
        if(type == 2){
            int l , r , x;
            cin >> l >> r >> x;
            r ++ ;
            while(true){
                int pl = get_max(1 , 1 , n + 1, l , r);
                if(a[pl] < x)
                    break ;
                a[pl] %= x;
                update(1 , 1 , n + 1 , pl);
            }
        }
        if(type == 3){
            int k , x;
            cin >> k >> x;
            a[k] = x;
            update(1 , 1 , n + 1 , k);
        }
    }
    return 0;
}
