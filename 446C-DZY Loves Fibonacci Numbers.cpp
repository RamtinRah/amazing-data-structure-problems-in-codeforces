

///link to the problem : https://codeforces.com/problemset/problem/446/C
/// a very very unique problem. the idea is the use sqrt decomposition on queries ! never seen before and probably will never see again





#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 3e5 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const int mod = 1e9+9;

int fib[maxn] , pref_fib[maxn] , a[maxn];
int num[maxn] , pref_sum[maxn];
vector<pii> v;
void calc(int n)
{
    a[1] = (a[1] + num[1]) % mod;
    pref_sum[1] = a[1];
    for(int i=2;i<=n;i++)
    {
        num[i] = (num[i] + (num[i-1] + num[i-2]) % mod ) % mod ;
        a[i] = (a[i] + num[i]) % mod;
        pref_sum[i] = (pref_sum[i-1] + a[i]) % mod;
    }
    v.clear();
    for(int i=1;i<=n;i++)
        num[i] = 0;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , m ;
    cin >> n >> m;
    fib[1] = pref_fib[1] = 1;
    for(int i=2;i<maxn;i++)
    {
        fib[i] = (fib[i-1] + fib[i-2]) % mod;
        pref_fib[i] = (pref_fib[i-1] + fib[i]) % mod;
    }
    for(int i=1;i<=n;i++)
    {
        cin >> a[i];
        pref_sum[i] = (pref_sum[i-1] + a[i]) % mod;
    }
    for(int i=0;i<m;i++)
    {
        if(i % 600 == 0)
            calc(n);
        int type; cin >> type;
        if(type == 1)
        {
            int l , r ;
            cin >> l >> r;
            v.pb(mp(l , r));
            num[l] = (num[l] + fib[1]) % mod;
            num[r + 1] = ( (num[r + 1] - fib[r - l + 2]) % mod + mod ) % mod;
            num[r + 2] = ( (num[r + 2] - fib[r - l + 1]) % mod + mod ) % mod;
        }
        else
        {
            int l , r ;
            cin >> l >> r;
            int ans = ((pref_sum[r] - pref_sum[l - 1]) % mod + mod) % mod;
            for(int j=0;j<v.size();j++)
            {
                int x = v[j].first , y = v[j].second;
                if(y < l || x > r)
                    continue;
                int k1 = max(l , x) - x + 1;
                int k2 = min(r , y) - x + 1;
                ans = ((ans + pref_fib[k2] - pref_fib[k1 - 1]) % mod + mod ) % mod;
            }
            cout << ans << endl;
        }
    }
    return 0;
}
