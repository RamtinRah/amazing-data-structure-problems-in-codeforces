/// link to problem : https://codeforces.com/problemset/problem/558/E

///A very complex segment tree problem that is great for teaching. the idea is not hard but the implementation is rather complex


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e5 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const int mod = 1e9+9;
string s;
int seg[30][4 * maxn] , lazy[4 * maxn];
int cnt[30];
void build(int ind , int l , int r)
{
    if(r - l < 2)
    {
        seg[s[l] - 'a'][ind] ++;
        return ;
    }
    int mid = (l + r) / 2;
    build(2 * ind , l , mid);
    build(2 * ind + 1 , mid , r);
    for(int i=0;i<26;i++)
        seg[i][ind] = seg[i][2 * ind] + seg[i][2 * ind + 1];
}
void shift(int ind , int l , int r)
{
    int mid = (l + r) / 2;
    int cap = mid - l;
    int p = 2 * ind;
    for(int i=0;i<26;i++){
        seg[i][2 * ind] = 0;
        seg[i][2 * ind + 1] = 0;
    }
    if(lazy[ind] == 1){
        for(int i=0;i<26;i++){
            int cur = seg[i][ind];
            if(cur <= cap){
                cap -= cur;
                seg[i][p] = cur;
            }
            else{
                seg[i][p] = cap;
                p ++ ;
                seg[i][p] = cur - cap;
                cap = mod;
            }
        }
    }
    if(lazy[ind] == 2){
        for(int i=25;i>=0;i--){
            int cur = seg[i][ind];
            if(cur <= cap){
                cap -= cur;
                seg[i][p] = cur;
            }
            else{
                seg[i][p] = cap;
                p ++ ;
                seg[i][p] = cur - cap;
                cap = mod;
            }
        }
    }
    lazy[2 * ind] = lazy[ind];
    lazy[2 * ind + 1] = lazy[ind];
    lazy[ind] = 0;
}
void get(int ind , int l , int r , int x , int y)
{
    if(y <= l || r <= x)
        return ;
    if(x <= l && r <= y)
    {
        for(int i=0;i<26;i++)
            cnt[i] += seg[i][ind];
        return ;
    }
    if(lazy[ind])
        shift(ind , l , r);
    int mid = (l + r) / 2;
    get(2 * ind , l , mid , x , y);
    get(2 * ind + 1 , mid , r , x , y);
}
void upd(int ind , int l , int r , int type)
{
    int cap = r - l;
    for(int i=0;i<26;i++)
        seg[i][ind] = 0;
    if(type == 1){
        for(int i=0;i<26;i++){
            if(cnt[i] <= cap){
                seg[i][ind] = cnt[i];
                cap -= cnt[i];
                cnt[i] = 0;
            }
            else{
                seg[i][ind] = cap;
                cnt[i] -= cap;
                cap = 0;
                break ;
            }
        }
    }
    if(type == 2){
        for(int i=25;i>=0;i--){
            if(cnt[i] <= cap){
                seg[i][ind] = cnt[i];
                cap -= cnt[i];
                cnt[i] = 0;
            }
            else{
                seg[i][ind] = cap;
                cnt[i] -= cap;
                cap = 0;
                break;
            }
        }
    }
    lazy[ind] = type;
}
void update(int ind , int l , int r , int x , int y , int type)
{
    if(y <= l || r <= x)
        return ;
    if(x <= l && r <= y)
    {
        upd(ind , l , r , type);
        return ;
    }
    if(lazy[ind])
        shift(ind , l , r);
    int mid = (l + r) / 2;
    update(2 * ind , l , mid , x , y , type);
    update(2 * ind + 1 , mid , r , x , y , type);
    for(int i=0;i<26;i++)
        seg[i][ind] = seg[i][2 * ind] + seg[i][2 * ind + 1];
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , q ;
    cin >> n >> q >> s;
    build(1 , 0 , n);
    while(q -- )
    {
        int l , r , type;
        cin >> l >> r >> type;
        type ^= 1 , l -- ;
        get(1 , 0 , n , l , r);
        update(1 , 0 , n , l , r , type + 1);
    }
    for(int i=0;i<n;i++)
    {
        get(1 , 0 , n , i , i+1);
        for(int j=0;j<26;j++)
        {
            if(cnt[j])
                cout << (char) (j + 'a');
            cnt[j] = 0;
        }
    }
    return 0;
}
