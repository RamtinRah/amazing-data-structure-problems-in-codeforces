/// link to problem : https://codeforces.com/problemset/problem/580/E
/// segment tree + hashing  again a very advanced problem that is very helpful for students to solve


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 1e5 + 5;
const ll inf = 1e18;
const ll mod = 1e9+7;
const ll base = 31;
ll seg[4 * maxn] , lazy[4 * maxn] , sum[maxn] , pw[maxn];
int n , m , k;
char s[maxn];
void build(int ind = 1 , int l = 0 , int r = n){
    if(r - l < 2){
        seg[ind] = s[l] - '0' + 1;
        return ;
    }
    int mid = (l + r) / 2;
    build(2 * ind , l , mid);
    build(2 * ind + 1 , mid , r);
    seg[ind] = (pw[r - mid] * seg[2 * ind] + seg[2 * ind + 1]) % mod;
}
void shift(int ind , int l , int r){
    if(!lazy[ind])
        return ;
    int mid = (l + r) / 2;
    int sz1 = mid - l;
    int sz2 = r - mid;
    seg[2 * ind] = lazy[ind] * sum[sz1 - 1];
    seg[2 * ind + 1] = lazy[ind] * sum[sz2 - 1];
    lazy[2 * ind] = lazy[ind];
    lazy[2 * ind + 1] = lazy[ind];
    lazy[ind] = 0;
}
void update(int ind , int l , int r , int x , int y , ll c){
    if(y <= l || r <= x)
        return ;
    if(x <= l && r <= y)
    {
        lazy[ind] = c;
        seg[ind] = (sum[r - l - 1] * c) % mod;
        return ;
    }
    shift(ind , l , r);
    int mid = (l + r) / 2;
    update(2 * ind , l , mid , x , y , c);
    update(2 * ind + 1 , mid , r , x , y , c);
    seg[ind] = (pw[r - mid] * seg[2 * ind] + seg[2 * ind + 1]) % mod;
}
ll hash1 , hash2 ;
void get1(int ind , int l , int r , int x , int y){
    if(y <= l || r <= x)
        return ;
    if(x <= l && r <= y){
        hash1 = (pw[r - l] * hash1 + seg[ind]) % mod;
        return ;
    }
    shift(ind , l , r);
    int mid = (l + r) / 2;
    get1(2 * ind , l , mid , x , y) ;
    get1(2 * ind + 1 , mid , r , x , y);
    seg[ind] = (pw[r - mid] * seg[2 * ind] + seg[2 * ind + 1]) % mod;
}
void get2(int ind , int l , int r , int x , int y){
    if(y <= l || r <= x)
        return ;
    if(x <= l && r <= y){
        hash2 = (pw[r - l] * hash2 + seg[ind]) % mod;
        return ;
    }
    shift(ind , l , r);
    int mid = (l + r) / 2;
    get2(2 * ind , l , mid , x , y) ;
    get2(2 * ind + 1 , mid , r , x , y);
    seg[ind] = (pw[r - mid] * seg[2 * ind] + seg[2 * ind + 1]) % mod;
}
void pre(){
    sum[0] = 1;
    pw[0] = 1;
    for(ll i=1;i<maxn;i++){
        pw[i] = (pw[i - 1] * base) % mod;
        sum[i] = (sum[i - 1] + pw[i]) % mod;
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    pre();
    scanf("%d%d%d" , &n , &m , &k);
    int q = m + k;
    scanf("%s" , s);
    build();
    while( q -- ){
        int type;
        scanf("%d" , &type);
        if(type == 1){
            int l , r , c;
            scanf("%d%d%d" , & l , & r , & c);
            l -- ;
            update(1 , 0 , n , l , r , c + 1);
        }
        else{
            int l , r , d;
            scanf("%d%d%d" , & l , & r , & d);
            l -- ;
            hash1 = 0 , hash2 = 0;
            get1(1 , 0 , n , l , r - d);
            get2(1 , 0 , n , l + d , r);
            if(hash1 == hash2)
                printf("YES\n");
            else
                printf("NO\n");
        }
    }
    return 0;
}
