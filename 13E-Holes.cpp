///link to problem : https://codeforces.com/problemset/problem/13/E




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e5 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const int mod = 1e9+7;
const int sz = 350;
int n , q , ar[maxn] , las[maxn] , cnt[maxn];
void tof(int i)
{
    if (i + ar[i] >= n || i / sz != (i + ar[i]) / sz ){
        las[i] = i;
        cnt[i] = 0;
    }
    else{
        las[i] = las[i+ar[i]];
        cnt[i] = cnt[i+ar[i]] + 1;
    }
}
int main()
{
    ios_base::sync_with_stdio(false);cin.tie(0);cout.tie(0);
    cin >> n >> q;
    for (int i=0;i<n;i++)
        cin >> ar[i];
    for (int i=n-1;i>=0;i--)
        tof(i);
    while( q -- )
    {
        int type , a , b;
        cin >> type;
        if (type)
        {
            cin >> a;
            a -- ;
            int last = 0 , count = 0;
            while(a < n){
                count += cnt[a] + 1;
                last = las[a];
                a = last + ar[last];
            }
            cout << last + 1 << " " << count <<'\n';
        }
        else
        {
            ///BADIHIE BADIHIE BADIHIE BADIHIE
            cin >> a >> b;
            a -- ;
            ar[a] = b;
            for(int i=a;i>-1;i--){
                if(i / sz != a / sz)
                    break;
                tof(i);
            }
        }
    }
}
