
///link to problem : https://codeforces.com/problemset/problem/383/C



#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 2e5 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const ll mod = 1e9+7;
int st[maxn] , fn[maxn] , a[maxn];
int depth[maxn];
bool d[maxn];
int seg[2][4 * maxn];
vector<int> edge[maxn];
int co;
void dfs(int v , int p)
{
    st[v] = co;
    for(int i=0;i<edge[v].size();i++)
    {
        int u = edge[v][i];
        if(u == p)
            continue;
        co ++ ;
        depth[u] = depth[v] + 1;
        dfs(u , v);
    }
    fn[v] = co + 1;
}
void update(int ind , int l , int r , int x , int y , int val)
{
    if(y <= l || r <= x)
        return ;
    if(x <= l && r <= y)
    {
        seg[d[x]][ind] += val;
        seg[d[x] ^ 1][ind] -= val;
        return ;
    }
    int mid = (l + r) / 2;
    update(2 * ind , l , mid , x , y , val);
    update(2 * ind + 1 , mid , r , x , y , val);
}
int sum = 0;
void get(int ind , int l , int r , int pos)
{
    sum += seg[d[pos]][ind];
    if(r - l < 2)
        return ;
    int mid = (l + r) / 2;
    if(pos < mid)
        get(2 * ind , l , mid , pos);
    else
        get(2 * ind + 1 , mid , r , pos);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , q;
    cin >> n >> q;
    for(int i=1;i<=n;i++)
        cin >> a[i];
    for(int i=0;i<n-1;i++)
    {
        int u , v;
        cin >> u >> v;
        edge[u].pb(v);
        edge[v].pb(u);
    }
    dfs(1 , 0);
    for(int i=1;i<=n;i++)
        d[st[i]] = depth[i] % 2;
    while(q --)
    {
        int type; cin >> type;
        if(type == 1)
        {
            int v , val;
            cin >> v >> val;
            update(1 , 0 , n , st[v] , fn[v] , val);
        }
        else
        {
            int v ; cin >> v;
            sum = 0;
            get(1 , 0 , n , st[v]);
            cout << sum + a[v] << endl;
        }
    }
    return 0;
}
