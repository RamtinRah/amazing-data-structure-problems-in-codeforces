

///link to the problem : https://codeforces.com/problemset/problem/396/C







#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 3e5 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const ll mod = 1e9+7;
int st[maxn] , fn[maxn] ;
ll seg1[4 * maxn] , seg2[4 * maxn] , seg3[4 * maxn];
int depth[maxn] , d[maxn];
vector<int> edge[maxn];
int co;
void dfs(int v , int p)
{
    st[v] = co;
    for(int i=0;i<edge[v].size();i++)
    {
        int u = edge[v][i];
        if(u == p)
            continue;
        co ++ ;
        depth[u] = depth[v] + 1;
        dfs(u , v);
    }
    fn[v] = co + 1;
}
void update(int ind , int l , int r , int x , int y , int val1 , int val2)
{
    if(y <= l || r <= x)
        return ;
    if(x <= l && r <= y)
    {
        seg1[ind] = (seg1[ind] + (ll)val1) % mod;
        seg2[ind] = (seg2[ind] + (ll)val2) % mod;
        seg3[ind] = (seg3[ind] + (ll)val2 * (ll)d[x]) % mod;
        return ;
    }
    int mid = (l + r) / 2;
    update(2 * ind , l , mid , x , y , val1 , val2);
    update(2 * ind + 1 , mid , r , x , y , val1 , val2);
}
ll ans = 0;
void get(int ind , int l , int r , int pos)
{
    ans = (ans + seg1[ind]) % mod;
    ans = (ans + seg3[ind]) % mod;
    ans = (ans - seg2[ind] * (ll)d[pos]) % mod;
    if(ans < 0)
        ans += mod;
    if(r - l < 2)
        return ;
    int mid = (l + r) / 2;
    if(pos < mid)
        get(2 * ind , l , mid , pos);
    else
        get(2 * ind + 1 , mid , r , pos);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n; cin >> n;
    for(int i=2;i<=n;i++)
    {
        int x; cin >> x;
        edge[x].pb(i);
        edge[i].pb(x);
    }
    dfs(1 , 0);
    for(int i=1;i<=n;i++)
        d[st[i]] = depth[i];
    int q ; cin >> q ;
    while( q -- )
    {
        int type ;
        cin >> type;
        if(type == 1)
        {
            int v , x , k;
            cin >> v >> x >> k;
            update(1 , 0 , n , st[v] , fn[v] , x , k);
        }
        else
        {
            int v; cin >> v;
            ans = 0;
            get(1 , 0 , n , st[v]);
            cout << ans << endl;
        }
    }
    return 0;
}
