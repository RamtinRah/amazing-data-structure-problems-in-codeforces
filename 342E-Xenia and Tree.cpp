///link to problem : https://codeforces.com/problemset/problem/342/E

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e5 + 5;
const int maxlog = 20;
const int mod = 1e9 + 7;
const int sq = 250;
int min_dis[maxn];
vector<int> querry , adj[maxn];
int n , qu;
int par[maxn][maxlog] , depth[maxn];
void dfs(int v , int p){
    par[v][0] = p;
    depth[v] = depth[p] + 1;
    min_dis[v] = depth[v] - 1;
    for(int i=1;i<maxlog;i++)
        par[v][i] = par[par[v][i-1]][i-1];
    for(int i=0;i<adj[v].size();i++){
        int u = adj[v][i];
        if(u != p)
            dfs(u , v);
    }
}
int lca(int u , int v){
    if(depth[u] > depth[v])
        swap(u , v);
    for(int i=maxlog-1;i>=0;i--){
        if(depth[par[v][i]] >= depth[u])
            v = par[v][i];
    }
    if(u == v)
        return v;
    for(int i=maxlog-1;i>=0;i--){
        if(par[v][i] != par[u][i]){
            v = par[v][i];
            u = par[u][i];
        }
    }
    return par[v][0];
}
void bfs(){
    int dis[maxn];
    for(int i=1;i<=n;i++)
        dis[i] = 0;
    queue<int> q;
    for(int i=0;i<querry.size();i++){
        int v = querry[i];
        dis[v] = 1;
        q.push(v);
    }
    querry.clear();
    while(! q.empty()){
        int v = q.front();
        q.pop();
        min_dis[v] = min(min_dis[v] , dis[v] - 1);
        for(int i=0;i<adj[v].size();i++){
            int u = adj[v][i];
            if(!dis[u]){
                dis[u] = dis[v] + 1;
                q.push(u);
            }
        }
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    cin >> n >> qu;
    for(int i=0;i<n-1;i++){
        int u , v;
        cin >> u >> v;
        adj[u].pb(v) , adj[v].pb(u);
    }
    dfs(1 , 0);
    while(qu -- ){
        if(querry.size() == sq)
            bfs();
        int type , v;
        cin >> type >> v;
        if(type & 1)
            querry.pb(v);
        else{
            int res = min_dis[v];
            for(int i=0;i<querry.size();i++){
                int u = querry[i];
                int l = lca(u , v);
                int d = depth[u] - depth[l] + depth[v] - depth[l];
                res = min(res , d);
            }
            cout << res << endl;
        }
    }
    return 0;
}
