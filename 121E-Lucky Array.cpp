///link to problem : https://codeforces.com/problemset/problem/121/E
///A truly amazing sqrt decomposition problem







#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<ll,ll> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e5 + 5 ;
const int max_num = 1e4 + 5;
const int maxlog = 20;
const int mod = 1e9 + 7;
const int sq = 350 ;
const ld pi = 3.14159265358979323846264;

int cnt[sq][max_num];
int val[sq] , a[maxn];
bool is_l[max_num];
char c[20];
vector<int> lucky;
bool is_lucky(int num){
    while(num){
        int d = num % 10;
        if(d != 7 && d != 4)
            return false;
        num /= 10;
    }
    return true;
}
void add(int l , int r , int v){
    int k = l / sq;
    while(l / sq == k && l <= r){
        cnt[k][a[l]] -- ;
        a[l] += v;
        cnt[k][a[l]] ++ ;
        l ++ ;
    }
    while(l + sq <= r){
        val[l / sq] += v;
        l += sq;
    }
    while(l <= r){
        cnt[l / sq][a[l]] -- ;
        a[l] += v;
        cnt[l / sq][a[l]] ++ ;
        l ++ ;
    }
}
int get(int l , int r){
    int k = l / sq;
    int res = 0;
    while(l / sq == k && l <= r){
        if(is_l[a[l] + val[l / sq]])
            res ++ ;
        l ++ ;
    }
    while(l + sq <= r){
        for(int i=0;i<lucky.size();i++){
            int x = lucky[i];
            if(x < val[l / sq])
                continue;
            res += cnt[l / sq][x - val[l / sq]];
        }
        l += sq;
    }
    while(l <= r){
        if(is_l[a[l] + val[l / sq]])
            res ++ ;
        l ++ ;
    }
    return res;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    int n , q;
    scanf("%d %d" , &n , &q);
    for(int i=0;i<n;i++){
        scanf("%d" , &a[i]);
        cnt[i / sq][a[i]] ++ ;
    }
    for(int i=1;i<max_num;i++){
        if(is_lucky(i)){
            lucky.pb(i);
            is_l[i] = true;
        }
    }
    while(q -- ){
        scanf("%s" , &c);
        if(c[0] == 'c'){
            int l , r;
            scanf("%d %d" , &l , &r);
            l -- , r -- ;
            printf("%d\n" , get(l , r));
        }
        else{
            int l , r , val;
            scanf("%d %d %d" , &l , &r , &val);
            l -- , r -- ;
            add(l , r , val);
        }
    }
    return 0;
}
