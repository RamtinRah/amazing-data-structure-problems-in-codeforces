///link to problem : https://codeforces.com/problemset/problem/292/E

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 1e5+6;
const ll inf = 1e18;
const int mod = 1e9+7;
int seg[4 * maxn] , t[4 * maxn];
int a[maxn] , b[maxn] , timer , ans ;

void update(int ind , int l , int r , int x , int y , int z ,int ti)
{
    if(y <= l || r <= x)
        return ;
    if(x <= l && r <= y)
    {
        seg[ind] = l + z - x  ;
        t[ind] = ti;
        return ;
    }
    int mid = (l + r) / 2;
    update(2 * ind , l , mid , x , y , z , ti);
    update(2 * ind + 1 , mid , r , x , y , z , ti);
}

void get(int ind , int l , int r , int pl)
{
    if(t[ind] > timer)
    {
        timer = t[ind];
        ans = pl - l + seg[ind];
    }
    if(r - l < 2)
        return ;
    int mid = (l + r) / 2;
    if(pl < mid)
        get(2 * ind , l , mid , pl);
    else
        get(2 * ind + 1 , mid , r , pl);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    for(int i=0;i<4*maxn;i++)
    {
        seg[i] = -1;
        t[i] = -1;
    }

    int n , m;
    cin >> n >> m;

    for(int i=0;i<n;i++)
        cin >> a[i];
    for(int i=0;i<n;i++)
        cin >> b[i];

    for(int i=0;i<m;i++)
    {
        int type;
        cin >> type;

        if(type == 1)
        {
            int x , y , k;
            cin >> x >> y >> k;
            x -- , y -- ;
            update(1 , 0 , n , y , y + k, x , i);
        }
        else
        {
            int x; cin >> x;
            x -- ;
            timer = -1;
            get(1 , 0 , n , x);
            if(timer == -1)
                cout << b[x] ;
            else
                cout << a[ans];
            cout << endl;
        }
    }
    return 0;
}
