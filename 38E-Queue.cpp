///link to problem : https://codeforces.com/contest/38/problem/G
///a very nice sqrt decomposition problem







#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e5 + 4;
const int maxlog = 22;
const ll inf = 1e18;
const int mod = 1e9 + 7;
const ll base = 29;
const int sq = 350;
int n , val[maxn] , maxi[maxn];
vector<vector<int> > v;
void reval(){
    vector<int> tof;
    for(int i=0;i<v.size();i++)
        for(int j=0;j<v[i].size();j++)
            tof.pb(v[i][j]);
    v.clear();
    int id = -1;
    for(int i=0;i<tof.size();i++){
        if(i % sq == 0){
            v.pb(vector<int>());
            id ++ ;
            maxi[id] = 0;
        }
        v[id].pb(tof[i]);
        maxi[id] = max(maxi[id] , tof[i]);
    }
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    cin >> n;
    v.pb(vector<int>());
    for(int i=1;i<=n;i++){
        int a , b;
        cin >> a >> b;
        if(i % sq == 0)
            reval();
        int j = v.size() - 1;
        while(true){
            if(maxi[j] > a || b < v[j].size() || j == 0){
                int k = v[j].size() - 1;
                v[j].pb(a);
                while(true){
                    if(k == -1 || b == 0 || v[j][k] > v[j][k + 1])
                        break;
                    swap(v[j][k] , v[j][k + 1]);
                    b -- ;
                    k -- ;
                }
                maxi[j] = max(maxi[j] , a);
                break;
            }
            else{
                b -= v[j].size();
                j -- ;
            }
        }
        val[a] = i;
    }
    for(int i=0;i<v.size();i++)
        for(int j=0;j<v[i].size();j++)
            cout << val[v[i][j]] << " ";
    return 0;
}
