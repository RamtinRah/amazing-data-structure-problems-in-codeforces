/// link to problem : https://codeforces.com/problemset/problem/380/C





#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 1e6+7;
const ll inf = 1e18;
const int mod = 1e9+7;
struct Node{
    int ans , open , close;
} seg[4 * maxn];
string s;
Node merge(Node a , Node b)
{
    Node c;
    c.ans = a.ans + b.ans + 2 * min(a.open , b.close);
    c.open = b.open;
    c.close = a.close;
    if(a.open < b.close)
        c.close += b.close - a.open;
    else
        c.open += a.open - b.close;
    return c;
}
void build(int ind , int l , int r)
{
    if(r - l < 2)
    {
        seg[ind].open = 0;
        seg[ind].ans = 0;
        seg[ind].close = 0;
        if(s[l] == '(')
            seg[ind].open = 1 ;
        else
            seg[ind].close = 1;
        //cout << l + 1 << " " << r  << " " << seg[ind].ans << " " << seg[ind].open << " " << seg[ind].close << endl;
        return ;
    }
    int mid = (l + r) / 2;
    build(2 * ind , l , mid);
    build(2 * ind + 1 , mid , r);
    seg[ind] = merge(seg[2 * ind] , seg[2 * ind + 1]);
    //cout << l + 1 << " " << r  << " " << seg[ind].ans << " " << seg[ind].open << " " << seg[ind].close << endl;
}
Node get(int ind , int l , int r , int x , int y)
{
    if(y <= l || r <= x)
        return {0 , 0 , 0};
    if(x <= l && r <= y)
        return seg[ind];
    int mid = (l + r) / 2;
    Node a = get(2 * ind , l , mid , x , y);
    Node b = get(2 * ind + 1 , mid , r , x , y);
    Node c = merge(a , b);
    return c;
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    cin >> s ;
    int n = s.size();
    build(1,0,n);
    int q; cin >> q;
    while(q -- )
    {
        int l , r; cin >> l >> r; l -- ;
        cout << get(1 , 0 , n , l , r).ans << endl;
    }
    return 0;
}
