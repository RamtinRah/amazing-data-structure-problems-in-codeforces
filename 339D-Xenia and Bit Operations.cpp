
///link to problem : https://codeforces.com/problemset/problem/339/D




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 3e6 + 4;
const ll inf = 1e18;
const int mod = 1e9+7;
int seg[4 * maxn] , a[maxn];
void build(int ind , int l , int r , bool b)
{
    if(r - l < 2)
    {
        seg[ind] = a[l];
        return ;
    }
    int mid = (l + r) / 2;
    build(2 * ind , l , mid , b ^ 1);
    build(2 * ind + 1 , mid , r , b ^ 1);
    if(b == 1)
        seg[ind] = seg[2 * ind] | seg[2 * ind + 1];
    else
        seg[ind] = seg[2 * ind] ^ seg[2 * ind + 1];
}
void update(int ind , int l , int r , bool b , int x , int val)
{
    if(r - l < 2)
    {
        seg[ind] = val;
        return ;
    }
    int mid = (l + r) / 2;
    if(x < mid)
        update(2 * ind , l , mid , b ^ 1 , x , val);
    else
        update(2 * ind + 1 , mid , r , b ^ 1 , x , val);
    if(b == 1)
        seg[ind] = seg[2 * ind] | seg[2 * ind + 1];
    else
        seg[ind] = seg[2 * ind] ^ seg[2 * ind + 1];
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    int n , m ; cin >> n >> m;
    bool b;
    if(n % 2 == 1)
        b = 1;
    else
        b = 0;
    n = 1 << n;
    for(int i=0;i<n;i++)
        cin >> a[i];
    build(1 , 0 , n , b);
    while(m -- )
    {
        int x , val;
        cin >> x >> val;
        x -- ;
        update(1 , 0 , n , b , x , val);
        cout << seg[1] << endl;
    }
    return 0;
}
