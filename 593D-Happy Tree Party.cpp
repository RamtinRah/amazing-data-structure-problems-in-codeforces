///link to problem : https://codeforces.com/problemset/problem/593/D



#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<ll,ll> pii;
typedef double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<ll>::iterator setit;

const ll maxn = 4e5 + 6;
const ll maxlog = 20;
const ll inf = 1e18;
const ll mod = 1e9 + 7;
const ll base = 307;
vector<ll> edge[maxn];
ll depth[maxn] , par[maxn][maxlog + 1];
vector<pii> edges;
ll w[maxn];
ll pr[maxn];
ll k[maxn];
void dfs(ll v , ll p)
{
    par[v][0] = p;
    depth[v] = depth[p] + 1;
    pr[v] = v;
    for(ll i=1;i<maxlog;i++)
    {
        ll u = par[v][i-1];
        par[v][i] = par[u][i-1];
    }
    for(ll i=0;i<edge[v].size();i++)
    {
        ll u = edge[v][i];
        if(u != p)
            dfs(u,v);
    }
}
ll lca(ll u,ll v)
{
    if(depth[u] < depth[v])
        swap(u,v);
    for(ll i=maxlog;i>=0;i--)
    {
        if(depth[par[u][i]] >= depth[v])
            u = par[u][i];
    }
    if(u == v)
        return u;
    for(ll i=maxlog;i>=0;i--)
    {
        if(par[u][i] != par[v][i])
        {
            u = par[u][i];
            v = par[v][i];
        }
    }
    return par[u][0];
}
ll get_par(ll v){
    if(v == pr[v])
        return v;
    return pr[v] = get_par(pr[v]);
}
void merge(ll u , ll v){
    ll pr1 = get_par(u);
    ll pr2 = get_par(v);
    if(pr1 != pr2)
        pr[u] = v;
}
ll y;
void get(ll v , ll l)
{
    if(y == 0)
        return ;
    v = get_par(v);
    ll u = par[v][0];
    if(depth[u] < depth[l])
        return ;
    y = y / k[v];
    get(u , l);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    ll n , q;
    cin >> n >> q;
    for(ll i=0;i<n-1;i++){
        ll u , v;
        ll we;
        cin >> u >> v >> we;
        edge[u].pb(v);
        edge[v].pb(u);
        edges.pb(mp(u , v));
        w[i] = we;
    }
    dfs(1 , 0);
    for(ll i=0;i<n-1;i++){
        ll u = edges[i].first;
        ll v = edges[i].second;
        if(w[i] == 1){
            if(depth[u] < depth[v])
                merge(v , u);
            else
                merge(u , v);
        }
        if(depth[u] < depth[v])
            k[v] = w[i];
        else
            k[u] = w[i];
    }
    while(q -- ){
        ll type;
        cin >> type;
        if(type & 1){
            ll a , b;
            cin >> a >> b >> y;
            ll v = lca(a , b);
            //cout << v << endl;
            get(a , v);
            get(b , v);
            cout << y << endl;
        }
        else{
            ll num; ll x;
            cin >> num >> x;
            num -- ;
            w[num] = x;
            ll u = edges[num].first , v = edges[num].second;
            if(w[num] == 1){
                if(depth[u] < depth[v])
                    merge(v , u);
                else
                    merge(u , v);
            }
            if(depth[u] < depth[v])
                k[v] = w[num];
            else
                k[u] = w[num];
        }
    }
    return 0;
}
