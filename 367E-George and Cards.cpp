
///link to the problem : https://codeforces.com/problemset/problem/387/E




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e6 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const ll mod = 1e9+7;

int p[maxn] , place[maxn];
bool bad[maxn];
set<int> st;
ll ans;
int seg[4 * maxn];
int get(int ind , int l , int r , int x , int y)
{
    if(y <= l || r <= x)
        return 0;
    if(x <= l && r <= y)
        return seg[ind];
    int mid = (l + r) / 2;
    return get(2 * ind , l , mid , x , y) + get(2 * ind + 1 , mid , r , x , y);
}
void update(int ind , int l , int r , int pl)
{
    seg[ind] ++;
    if(r - l < 2)
        return ;
    int mid = (l + r) / 2;
    if(pl < mid)
        update(2 * ind , l , mid , pl);
    else
        update(2 * ind + 1 , mid , r , pl);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n , k ;
    cin >> n >> k;
    for(int i=1;i<=n;i++)
    {
        cin >> p[i];
        place[p[i]] = i;
    }
    for(int i=0;i<k;i++)
    {
        int x; cin >> x;
        bad[x] = true;
    }
    st.insert(0);
    st.insert(n + 1);
    for(int i=1;i<=n;i++)
    {
        if(bad[i])
        {
            st.insert(place[i]);
            continue;
        }
        st.insert(place[i]);
        setit it = st.find(place[i]);
        int l , r;
        it -- ;
        l = *it + 1;
        it ++ ;
        it ++ ;
        r = *it;
        ans += (ll)r - (ll)l - (ll)get(1 , 1 , n + 1 , l , r);
        update(1 , 1 , n + 1 , place[i]);
        st.erase(place[i]);
    }
    cout << ans;
    return 0;
}
