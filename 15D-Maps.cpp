///link to problem : https://codeforces.com/problemset/problem/15/D
/// very scary when you want to start implementation but when you start it is really not that hard


#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<ll,ll>::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e3 + 4;
const int maxlog = 22;
const ll inf = 1e18;
const int mod = 1e9 + 7;
const ll base = 307;
int n , m , x , y;
ll num[maxn][maxn] , pref_sum[maxn][maxn] , sum[maxn][maxn] , ans_sum[maxn][maxn];
ll mini[maxn][maxn] , ans_min[maxn][maxn];
bool mark[maxn][maxn];
vector<pair<ll,pii> > v;
vector<pair<pii,ll> > ans;
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);cout.precision(20);
    scanf("%d%d%d%d" , & n , & m , & x , & y);
    for(int i=1;i<=n;i++){
        for(int j=1;j<=m;j++){
            scanf("%I64d" , & num[i][j]);
            pref_sum[i][j] = pref_sum[i][j - 1] + num[i][j];
        }
    }
    for(int i=1;i<=n;i++){
        for(int j=1;j<=m-y+1;j++){
            sum[i][j] = pref_sum[i][j + y - 1] - pref_sum[i][j - 1];
            sum[i][j] += sum[i - 1][j];
        }
    }
    for(int i=1;i<=n-x+1;i++){
        for(int j=1;j<=m-y+1;j++){
            ans_sum[i][j] = sum[i + x - 1][j] - sum[i - 1][j];
            //cout << i << " " << j << " " << ans_sum[i][j] << endl;
        }
    }
    for(int i=1;i<=n;i++){
        multiset<ll> st;
        for(int j=m;j>m-y+1;j--)
            st.insert(num[i][j]);
        for(int j=m-y+1;j>=1;j--){
            st.insert(num[i][j]);
            mini[i][j] = *st.begin();
            st.erase(st.find(num[i][j + y - 1]));
        }
    }
    for(int j=1;j<=m-y+1;j++){
        multiset<ll> st;
        for(int i=n;i>n-x+1;i--)
            st.insert(mini[i][j]);
        for(int i=n-x+1;i>=1;i--){
            st.insert(mini[i][j]);
            ans_min[i][j] = *st.begin();
            st.erase(st.find(mini[i + x - 1][j]));
        }
    }
    for(int i=1;i<=n-x+1;i++){
        for(int j=1;j<=m-y+1;j++){
            ll z = ans_sum[i][j] - ans_min[i][j] * (ll) x * (ll) y;
            v.pb(mp(z , mp(i , j)));
        }
    }
    sort(v.begin() , v.end());
    for(int i=0;i<v.size();i++){
        int a = v[i].second.first;
        int b = v[i].second.second;
        if(!mark[a][b] && !mark[a + x - 1][b] && !mark[a][b + y - 1] && !mark[a + x - 1][b + y - 1]){
            ans.pb(mp(mp(a , b) , v[i].first));
            for(int j=a;j<=a+x-1;j++)
                for(int k=b;k<=b+y-1;k++)
                    mark[j][k] = true;
        }
    }
    printf("%d\n" , ans.size());
    for(int i=0;i<ans.size();i++)
        printf("%d %d %I64d\n" , ans[i].first.first , ans[i].first.second , ans[i].second);
    return 0;
}
