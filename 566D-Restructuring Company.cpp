///link to problem : https://codeforces.com/problemset/problem/566/D

#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<int,int>::iterator mapit;
typedef set<int>::iterator setit;
const int maxn=2e5+6;
const int inf=1e18;
set<int> st;
int par[maxn];
int get_par(int v)
{
    if(par[v] == v)
        return v;
    return par[v] = get_par(par[v]);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);
    int n,q; cin >> n >> q;
    for(int i=1;i<=n;i++)
    {
        st.insert(i);
        par[i] = i;
    }
    while(q--)
    {
        int type; cin >> type;
        if(type == 1)
        {
            int x,y; cin >> x >> y;
            int p1 = get_par(x);
            int p2 = get_par(y);
            if(p1 != p2)
                par[p1] = p2;
        }
        if(type == 2)
        {
            int x,y; cin >> x >> y;
            setit it1 = st.lower_bound(x);
            vector<int> ToErase;
            for(setit it = it1 ; it != st.end() ; it ++)
            {
                if(*it > y)
                    break;
                int z = *it;
                ToErase.pb(z);
                int p1 = get_par(z);
                int p2 = get_par(x);
                if(p1 != p2)
                    par[p1] = p2;
            }
            for(int i=0;i<ToErase.size();i++)
                st.erase(ToErase[i]);
            st.insert(x);
            st.insert(y);
        }
        if(type == 3)
        {
            int x,y; cin >> x >> y;
            if(get_par(x) == get_par(y))
                cout << "YES";
            else
                cout << "NO";
            cout << endl;
        }
    }
    return 0;
}
