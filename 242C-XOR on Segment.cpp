
///link to problem : https://codeforces.com/problemset/problem/242/E











#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back

using namespace std;

typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;

const int maxn = 1e5 + 5;
const int maxlog = 22;
const ll inf = 1e18;
const ll mod = 1e9+7;
int seg[maxlog][4 * maxn];
bool lazy[maxlog][4 * maxn];
int a[maxn];
void build(int ind , int l , int r)
{
    if(r - l < 2)
    {
        for(int i=0;i<maxlog;i++)
            if(a[l] & (1<<i))
                seg[i][ind] ++;
        return ;
    }
    int mid = (l + r) / 2;
    build(2 * ind , l , mid);
    build(2 * ind + 1 , mid , r);
    for(int i=0;i<maxlog;i++)
        seg[i][ind] = seg[i][2 * ind] + seg[i][2 * ind + 1];
}
void upd(int i , int ind , int l , int r)
{
    seg[i][ind] = r - l - seg[i][ind];
    lazy[i][ind] ^= 1;
}
void shift(int i , int ind, int l , int r)
{
    int mid = (l + r) / 2;
    upd(i , 2 * ind , l , mid);
    upd(i , 2 * ind + 1 , mid , r);
    lazy[i][ind] = 0;
}
int get(int i , int ind , int l , int r , int x , int y)
{
    if(y <= l || r <= x)
        return 0;
    if(x <= l && r <= y)
        return seg[i][ind];
    if(lazy[i][ind])
        shift(i , ind , l , r);
    int mid = (l + r) / 2;
    return get(i , 2 * ind , l , mid , x , y) + get(i , 2 * ind + 1 , mid , r , x , y);
}
void update(int i , int ind , int l , int r , int x , int y)
{
    if(y <= l || r <= x)
        return ;
    if(x <= l && r <= y)
    {
        upd(i , ind , l , r);
        return ;
    }
    if(lazy[i][ind])
        shift(i , ind , l , r);
    int mid = (l + r) / 2;
    update(i , 2 * ind , l , mid , x , y);
    update(i , 2 * ind + 1, mid , r , x , y);
    seg[i][ind] = seg[i][2 * ind] + seg[i][2 * ind + 1];
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n; cin >> n;
    for(int i=0;i<n;i++)
        cin >> a[i];
    build(1 , 0 , n);
    int m ; cin >> m;
    while(m -- )
    {
        int type; cin >> type;
        if(type == 1)
        {
            int l , r ; cin >> l >> r;
            l -- ;
            ll res = 0;
            for(int i=0;i<maxlog;i++)
                res += (ll)get(i , 1 , 0 , n , l , r) * (ll)(1<<i);
            cout << res << endl;
        }
        else
        {
            int l , r , x;
            cin >> l >> r >> x;
            l -- ;
            for(int i=0;i<maxlog;i++)
                if(x & (1<<i))
                    update(i , 1 , 0 , n , l , r);
        }
    }
    return 0;
}
