///link to the problem : https://codeforces.com/problemset/problem/459/D




#include <bits/stdc++.h>
#define mp make_pair
#define pb push_back
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef long double ld;
typedef map<string,vector<string> >::iterator mapit;
typedef set<int>::iterator setit;
const int maxn = 1e6 + 6;
const int maxlog = 21;
const ll inf = 1e18;
const int mod = 1e9+7;
int seg[maxlog][maxn];
map<int,int> cnt;
int le[maxn] , ri[maxn] , a[maxn];
void build(int level , int x , int y)
{
    if(y - x < 2)
    {
        seg[level][x] = ri[x];
        return ;
    }
    int mid = (x + y) / 2;
    build(level + 1 , x , mid);
    build(level + 1 , mid , y);
    merge(seg[level + 1] + x , seg[level + 1] + mid , seg[level + 1] + mid , seg[level + 1] + y , seg[level] + x);
}
int get(int level , int l , int r , int x , int y , int val)
{
    if(x <= l && r <= y)
    {
        int ans = lower_bound(seg[level] + l , seg[level] + r , val) - (seg[level] + l);
        return ans ;
    }
    if(y <= l || r <= x)
        return 0;
    int mid = (l + r) / 2;
    return get(level + 1 , l , mid , x , y , val) + get(level + 1 , mid , r , x , y , val);
}
int main()
{
    ios_base::sync_with_stdio(0);cin.tie(0);cout.tie(0);
    int n; cin >> n;
    for(int i=0;i<n;i++)
    {
        cin >> a[i];
        cnt[a[i]] ++ ;
        le[i] = cnt[a[i]];
    }
    cnt.clear();
    for(int i=n-1;i>=0;i--)
    {
        cnt[a[i]] ++ ;
        ri[i] = cnt[a[i]];
    }
    build(0 , 0 , n);
    ll ans = 0;
    for(int i=0;i<n;i++)
        ans += (ll)get(0 , 0 , n , i + 1 , n , le[i]);
    cout << ans;
    return 0;
}
